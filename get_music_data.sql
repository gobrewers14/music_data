------------------------------------------------------------------------------
-- @author:
-- 		john martinez <john.r.martinez14@gmail.com>
-- @date:
-- 		2017-09-19	12:38:23
-- @description:
-- 		Postgres query to extract artist, song, and album from database.
-- @example:
-- 		https://stackoverflow.com/questions/1517635/save-pl-pgsql-output-from-postgresql-to-a-csv-file
-- 		musicbrainz_db=>\f '\t'
-- 		musicbrainz_db=>\a 
-- 		musicbrainz_db=>\o '/tmp/music_data.tsv'
-- 		musicbrainz_db=>[copy and paste query from above]
-- 		musicbrainz_db=>\q
------------------------------------------------------------------------------

SELECT DISTINCT artist_name
  , song_name
  , album_name
FROM (
	SELECT artist_name
	  , song_name
	  , r.name AS album_name
	  , r.release_group
	FROM (
		SELECT ac.name AS artist_name
		  , t.name AS song_name
		  , m.release
		FROM track t
		JOIN medium m
		ON m.id=t.medium
		JOIN artist_credit ac
		ON ac.id=t.artist_credit ) x
	JOIN release r
	ON r.id=x.release ) y
JOIN release_group rg
ON y.release_group=rg.id
WHERE rg.type=1;