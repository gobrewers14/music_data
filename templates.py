# -*- coding: utf-8 -*-

"""
@author:    John Martinez <john.martinez@toyotaconnected.com"
@date:      2017-09-21  10:12:25
"""

raw_beginnings = [
    "lexi play some",
    "play",
    "will you play",
    "could you play",
    "i want to hear",
    "play some",
    "rock some",
    "lexi how about some",
    "play me some"
]

musicaction = [
    "play",
    "rock",
    "how about",
    "hear"
]

endings = [
    "can you play that?",
    "in my car",
    "out of my speakers",
    "with spotify",
    "using spotify",
    "on spotify",
    "right now",
    "please",
    "really loud",
    "at a decent volume",
    "thanks",
    "",
    "",
    "",
    ""
]

music_map = {
    "artist": "by",
    "album": "from",
}

possible_combs = [
    "artist",
    "title",
    "album",
    # "artist_title",
    # "artist_album",
    "title_album",
    "title_artist",
    "artist_title_album",
    "title_artist_album",
    "title_album_artist"
]

out_entities = {
    "start": 0,
    "end": 0,
    "value": '',
    "entity": ''
}

def generate_beginnings(begin, music, entities):
    """
    parameters:
    ----------
    begin - a list of raw beginnings
    music - a list of music actions

    returns:
    -------
    a dictionary of tuples.  {index: (actual string, formatted string)}
    """

    out = {}
    for k, b in enumerate(begin):
        entities = {
            "start": 0,
            "end": 0,
            "value": '',
            "entity": ''
        }
        for m in music:
            if m in b:
                start = b.index(m)
                end = start + len(m)
                entities['start'] = start
                entities['end'] = end
                entities['value'] = m
                entities['entity'] = "musicaction"

        out[k] = (b, entities)
    return out

beginnings = generate_beginnings(raw_beginnings, musicaction, out_entities)
