#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author:    John Martinez <john.martinez@toyotaconnected.com>
@date:      2017-09-20  16:30:28
"""

import random
import utils
import json
import config
import pandas as pd
from templates import beginnings, endings, music_map, possible_combs
from sklearn.utils import shuffle
from multiprocessing import Pool, cpu_count
from argparse import ArgumentParser


# prelims
random.seed(config.RANDOM_SEED)
WORKING_DIR = utils._get_working_dir(__file__)


class GenerateData:
    """
    This class injests data of the form

        artist | title | album

    and creates training data to be passed to rasa for training.
    """
    def __init__(self, path, logger, num_samples=1000, verbose=True,
                 do_mp=True):
        self.path = path
        self.logger = logger
        self.verbose = verbose
        self.do_mp = do_mp
        self.num_samples = num_samples
        self.dataframe_path = "{}/{}/{}".format(WORKING_DIR, "data", self.path)
        if self.num_samples < 10:
            self.update_when = -int(1e7)
        else:
            self.update_when = self.num_samples // 10
        if self.verbose:
            self.logger.info("=" * 79)
            self.logger.info("INITIALIZE")
            self.logger.info("\t{}".format(self.__str__()))
            self.logger.info("=" * 79)

    def __str__(self):
        return "<data_gen_{}_samples_{}>".format(self.num_samples,
                                                utils._timestamp())
    @staticmethod
    def _stringify(s):
        """
        check if something is a string, if it is, return it , if not
        make it a string and return it.

        parameters:
        ----------
        s - a string

        returns:
        -------
        s - a string
        """
        s = s if isinstance(s, str) else str(s)
        return s.lower()

    def gen_instance(self, args):
        """
        create an object that can be injested by rasa for training.

        parameters:
        ----------
          args - a tuple containing (int, dict).  One occurance of
          artist/title/album from the training dataset music.tsv and its
          index.

        returns:
        -------
          out_obj - a dictionary
        """

        idx, instance = args
        if self.verbose:
            #  do an update
            if (idx + 1) % self.update_when == 0 and idx > 0:
                update = "\t@ iter: {}\ttime: {}".format(
                    str(idx+1), utils._timestamp())
                self.logger.info(update)

        # pick a random beginning
        beginning = random.choice(beginnings)

        # pick a random ending
        ending = random.choice(endings)

        # pick a random artist/title/album combination
        r_choice = random.choice(possible_combs)
        keys_to_extract = r_choice.split('_')

        # add the beginning to the string
        instance_str = [beginning[0]]

        # if we are using more than one entity
        # we will need to add words like `from` to the
        # beginning of album names and `by` to the
        # beginning of artist names.
        if len(keys_to_extract) > 1:
            for ks in keys_to_extract:
                if music_map.get(ks):
                    instance_str.append(music_map[ks])

                instance_str.append(self._stringify(instance[ks]))
        else:
            instance_str.append(self._stringify(instance[keys_to_extract[0]]))

        # add the ending of the string
        instance_str.append(ending)

        out_string = " ".join(instance_str)
        out_obj = {
            "text": '',
            "intent": "music.play",
            "entities": []
        }
        # append the final string to the text field
        out_obj['text'] = out_string

        # append the musicaction entitiy
        out_obj['entities'].append(beginning[1])

        # construct and append remanining entities
        for ks in keys_to_extract:
            out_entity_tmp = {}
            value = self._stringify(instance[ks])
            start = out_string.index(value)
            out_entity_tmp['start'] = start
            out_entity_tmp['end'] = start + len(value)
            out_entity_tmp['value'] = value
            out_entity_tmp['entity'] = ks

            out_obj['entities'].append(out_entity_tmp)

        return out_obj

    def _import_data(self, do_shuffle=True):
        """
        load the data from the text file specified in the `path`
        class parameter.

        parameters:
        ----------
        do shuffle - a bool.  Do you want to shuffle the training data or not?

        returns:
        -------
        dataframe - a pandas dataframe object.

        """
        if self.verbose:
            self.logger.info("import dataset")
            self.logger.info("")
        dataframe = pd.read_csv(self.dataframe_path, sep='\t')
        if do_shuffle:
            dataframe = shuffle(dataframe)
        dataframe = dataframe.head(self.num_samples)
        dataframe = dataframe.reset_index(drop=True)
        return dataframe

    def _initialize_mp(self):
        """
        if doing multiprocessing, this creates a pool instances and configures
        the number of cores on you machine.

        parameters:
        ----------
        None

        returns:
        -------
        pool - a multiprocessing.Pool object
        """
        if self.verbose:
            self.logger.info("initializing multiprocessing module")
            self.logger.info("")
        cores = cpu_count() - 1
        pool = Pool(processes=cores)
        return pool

    @staticmethod
    def _format_output(a_list_of_objs):
        """
        format the output we generate so that it can be injested by rasa.

        parameters:
        ----------
        a_list_of_objs - a list of dicts, all the training instances we've
                         generated.

        returns:
        -------
        rasa_dic - a dict object
        """
        rasa_dic = {"rasa_nlu_data": {"common_examples": []}}
        rasa_dic['rasa_nlu_data']['common_examples'] = a_list_of_objs
        return rasa_dic

    def _saver(self, data, out_path):
        """ doc string. """
        if self.verbose:
            self.logger.info("saving generated data to {}".format(out_path))
            self.logger.info("")
        try:
            with open(out_path, 'w') as json_file:
                json.dump(data, json_file)
        except:
            self.logger.info(
                "couldn't save output file to {}".format(out_path))
            self.logger.info("")

        return None

def run(options):
    """ run an instance of GenerateData(). """
    logname = "generate_data_{}.log".format(utils._timestamp())
    logger = utils._get_logger(WORKING_DIR + "/log", logname)

    gd = GenerateData(
        path=options.path,
        logger=logger,
        num_samples=options.samples,
        verbose=options.verbose,
        do_mp=False
    )

    # get the data
    data = gd._import_data(do_shuffle=options.shuffle)

    if gd.do_mp:
        pool = gd._initialize_mp()
        output = pool.map(gd.gen_instance, data.iterrows())

    else:
        output = []
        for inst in data.iterrows():
            output.append(gd.gen_instance(inst))

    # save data
    output = gd._format_output(output)  # probably change this
    gd._saver(output, options.outfile)
    gd.logger.info("complete!")

def parse_args(parser):
    """ parse the command line arguments for this script. """
    parser.add_argument("--path", type=str, dest="path", default="music.tsv",
                        help="the path to the input data")
    parser.add_argument("--samples", type=int, dest="samples", default=100000,
                        help="how many samples do you want to generate")
    parser.add_argument("--shuffle", action="store_true", dest="shuffle",
                        help="shuffle the input data")
    parser.add_argument("--verbose", action="store_true", dest="verbose",
                        help="logs information")
    parser.add_argument("--no-shuffle", action="store_false", dest="shuffle",
                        help="dont shuffle the input data")
    parser.add_argument("--no-verbose", action="store_false", dest="verbose",
                        help="dont log information")
    # parser.add_argument("--multiprocessing")
    parser.add_argument("--outfile", type=str, dest="outfile",
                        default="music_training_data.json",
                        help="place where you want to store the output file")
    parser.set_defaults(shuffle=True, verbose=True)

    options = parser.parse_args()
    return options

if __name__ == "__main__":
    parser = ArgumentParser(description="add command line arguements")
    options = parse_args(parser)
    run(options)
