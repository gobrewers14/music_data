# -*- coding: utf-8 -*-

import os
import platform

PLATFORM = platform.system()

RANDOM_SEED = 1234567890987654321

ROOT_DIR = ''
WORK_DIR = ''
DATA_DIR = ''
LOG_DIR = ''
