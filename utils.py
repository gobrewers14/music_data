# -*- coding: utf-8 -*-

"""
@author:    John Martinez <john.martinez@toyotaconnected.com>
@date:      2017-09-21  10:14:47
"""

import os
import logging
import logging.handlers
import time


def _timestamp():
    """ generate local time stamp. """
    return time.strftime("%Y%m%d-%H%M%S", time.localtime())

def _get_working_dir(filename):
    """
    extract the current working directory regardless of where function
    is run from.
    """
    return os.path.dirname(os.path.abspath(filename))

def _get_logger(log_dir, log_name, log_level=logging.INFO):
    """ setup logger for logging. """
    fmt = "[%(asctime)s] %(levelname)s: %(message)s"
    formatter = logging.Formatter(fmt)

    handler = logging.handlers.RotatingFileHandler(
        filename=os.path.join(log_dir, log_name),
        maxBytes=10 * 1024 * 1024,
        backupCount=10
    )
    handler.setFormatter(formatter)

    logger = logging.getLogger("")
    logger.addHandler(handler)
    logger.setLevel(log_level)
    return logger
